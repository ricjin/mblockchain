


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.io.*;

public class mcoin {

	

	//list of transaction in list in the order of oldest to latest transaction
			
	public static void main(String[] args)  {
		Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
	   
	}

	private static void getHTMLContent()  {
		
		Scanner in = new Scanner(System.in);
		int testCases = Integer.parseInt(in.nextLine());
		String regex = "<([a-zA-Z][a-zA-Z0-9]*)\\b[^>]*>(.*?)</\\1>";
        Pattern p = Pattern.compile(regex);
       
		
	     while(testCases>0){
			String line = in.nextLine();
			System.out.println(line.replace(regex, "TTT"));
			 Matcher m =  p.matcher(line);
		      if (m.find( )) {
		         System.out.println("Found value: " + m.group(0) );
		         System.out.println("Found value: " + m.group(1) );
		         System.out.println("Found value: " + m.group(2) );
		    
		      }else {
		         System.out.println("NO MATCH");
		      }
          	//Write your code here
							
			testCases--;
		}
		
	  }
	
	
	private static void findDuplicate()
	  {
		String regex = "\\b(\\w+)(\\b\\s+\\b\\1\\b)*";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        
        Scanner in = new Scanner(System.in);
        int numSentences = Integer.parseInt(in.nextLine());
        
        while (numSentences-- > 0) {
            String input = in.nextLine();
            
            Matcher m = p.matcher(input);
            
            // Check for subsequences of input that match the compiled pattern
            while (m.find()) {
                input = input.replaceAll(m.group(0),m.group(1));
            }
            
            // Prints the modified sentence.
            System.out.println(input);
        }
        
        in.close();
	  }


	static boolean isAnagram(String a, String b) {
	        // Complete the function
		if ((a.length() >= 50) || (b.length() >= 50))
			return false;
		final Pattern pattern = Pattern.compile("[A-Za-z]+");
        if (!(pattern.matcher(a).matches()) ||
        	!(pattern.matcher(b).matches())) {
        	return false;
        }
		
        char[] aa = a.toCharArray();
        char[] bb = b.toCharArray();
        
        if (aa.length != bb.length)
          {
        	return false;
          }
        
        Arrays.sort(aa);
        Arrays.sort(bb);
        
        for (int i=0;i<aa.length;i++)
          {
        	if (Character.toUpperCase(aa[i]) !=
        		Character.toUpperCase(bb[i]))
        	  {
        		return false;
        	  }
          }
        return true;
	  }






	private static void tokener() {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
                
        final Pattern pattern = Pattern.compile("[A-Za-z!,?._'@\\s]+");
        if (!pattern.matcher(s).matches()) {
           System.err.println("Invalid String");
        }
        
        // Write your code here.
        String[] strs = s.split("[^A-Za-z]+");
        System.out.println(strs.length);
        for (String ss : strs)
          {
        	System.out.println(ss);
          }
        
        scan.close();
	}

	public static int generalizedGCD(int num, int[] arr)
    {
		HashMap mMap = new HashMap();
		int cSize;
		int GCD =0;
        // WRITE YOUR CODE HERE
        for (int i=0;i<arr.length;i++)
          {
        	for (int n=arr[i];n>0;n--)
	   	     {
	   		   if (arr[i]%n ==0)
	   		     {
	   			   if (mMap.containsKey(n))
	   			     {
	   				   cSize = (int)mMap.get(n);
	   			     }
	   			   else
	   			     {
	   				   cSize = 0;
	   			     }
	   			   mMap.put(n, cSize+1);
	   		     }
	   	     }
          }
        for (Object key : mMap.keySet()) {
          if (mMap.get(key).equals(num) && ((int)key>GCD))
            {
        	  GCD = (int)key;
            }
			
        }
		return GCD;
        
    }
	
	private static List<Integer> findDivisor(int num) {
		
	   List<Integer> r = new ArrayList<Integer>();
	   
	   for (int n=num;n>0;n--)
	     {
		   if (num%n ==0)
		     {
			   r.add(n);
		     }
	     }
	   return r;
	   
	}
	
	public static List<Integer> cellCompete(int[] states, int days)
    {
      List<Integer> Istates  = new ArrayList<Integer>();
      for (int i=0;i<states.length;i++)
      {
    	  Istates.add(new Integer(states[i]));
      }
      for (int i=1;i<=days;i++)
        {
          Istates = runStates(Istates);    
        }
      return Istates;
        
    }
    
    private static List<Integer> runStates(List<Integer> states)
    {
       //add padding
        List<Integer> paddedStates = new ArrayList<Integer>();
       paddedStates.add(new Integer(0));
      
       
       for (int i=0;i<states.size();i++)
         {
           paddedStates.add(states.get(i));        
         }
       paddedStates.add(new Integer(0));
       
       //actual cell compete
       List<Integer> output = new ArrayList<Integer>();
       
       //find states
       for (int j=1;j<paddedStates.size()-1;j++)
         {
           output.add(new Integer((paddedStates.get(j-1).equals(paddedStates.get(j+1)))?0:1));    
         }
         
     
       return output;
    }
    
	private static void tester() throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Pay to : ");
        String payTo = br.readLine();
        int amount = 0;
    	
    	System.out.println("How much:");
        try{
            amount = Integer.parseInt(br.readLine());
        
        }catch(NumberFormatException nfe){
            System.err.println("Invalid Format!");
        }
        
        ByteBuffer bf = ByteBuffer.allocate(Integer.SIZE / 8);;
        bf.putInt(amount);
        
        byte[] amountArr = bf.array();
        int x = bf.getInt();
        System.out.printf("%i ", x);
	}
	
   
	
	
	
	
	
}
