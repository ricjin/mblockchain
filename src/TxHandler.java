
import java.util.ArrayList;

public class TxHandler {

	
	private UTXOPool newUtxoPool;
    /**
     * Creates a public ledger whose current UTXOPool (collection of unspent transaction outputs) is
     * {@code utxoPool}. This should make a copy of utxoPool by using the UTXOPool(UTXOPool uPool)
     * constructor.
     */
    public TxHandler(UTXOPool utxoPool) {
        // newUtxoPool THIS
    	newUtxoPool = new UTXOPool(utxoPool);
    }

    /**
     * @return true if:
     * (1) all outputs claimed by {@code tx} are in the current UTXO pool, 
     * (2) the signatures on each input of {@code tx} are valid, 
     * (3) no UTXO is claimed multiple times by {@code tx},
     * (4) all of {@code tx}s output values are non-negative, and
     * (5) the sum of {@code tx}s input values is greater than or equal to the sum of its output
     *     values; and false otherwise.
     */
    public boolean isValidTx(Transaction tx) {
        // IMPLEMENT THIS
    	return true;
    }

    /**
     * Handles each epoch by receiving an unordered array of proposed transactions, checking each
     * transaction for correctness, returning a mutually valid array of accepted transactions, and
     * updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        
    	int transactionSize = possibleTxs.length;
    	
    	Transaction[] acceptedTxs = new Transaction[ transactionSize ];
    	int i = 0;
    	
    	ArrayList<UTXO> myList = newUtxoPool.getAllUTXO();
    	int index = 0;
    	
    	// assume that array of transaction is organized by time and 
    	// iteration is done from oldest to newest.
		for (Transaction t : possibleTxs)
	      {
	    	 if (isValidTx(t))
	    	   {
	    		 acceptedTxs[i++] = t; 
	    		 byte[] txHash = {1,2,3};
	    		 UTXO utxo = new UTXO(txHash, index);
	    		 newUtxoPool.addUTXO(utxo, t.getOutput(index));
	    	   }
	      }
		
		
		return acceptedTxs;
		
    }

}
